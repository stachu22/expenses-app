import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import { addExpense } from './actions/expenses';
import './index.css';
import AppRouter from './routers/AppRouter';
import registerServiceWorker from './registerServiceWorker';

const store = configureStore();

store.dispatch(addExpense({ description: 'Water bill', amount: 23 }));
store.dispatch(addExpense({ description: 'Poo', amount: 240, createdAt: 1000 }));
store.dispatch(addExpense({ description: 'Rent', amount: 23243 }));

const jsx = (
  <Provider store={store}>
    <AppRouter />
  </Provider>
);

ReactDOM.render(jsx, document.getElementById('root'));
registerServiceWorker();

