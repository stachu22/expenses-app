import React from 'react';
import { Link } from 'react-router-dom';

const ExpenseListItem = ({ id, description, amount, note }) => (
    <div>
        <Link to={`/edit/${id}`}>
            <p>Description: {description}</p>
        </Link>
        <p>Amount: {amount}</p>
        <p>Note: {note}</p>
    </div>
);


export default ExpenseListItem;
