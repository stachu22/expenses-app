import React from 'react';
import moment from 'moment';
import { SingleDatePicker } from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';
import 'react-dates/initialize';

class ExpenseForm extends React.Component {
    constructor(props) {
        super(props)
        
        this.state = {
            description: props.expense ? props.expense.description : '',
            amount: props.expense ? (props.expense.amount /100).toString() : '',
            note: props.expense ? props.expense.note : '',
            createdAt: props.ecpense ? moment(props.expense.createdAt): moment(),
            calenderFocused: false,
            error: ''
        };

    }
    onDescriptionChange = (event) => {
        const description = event.target.value;
        this.setState(() => ({ description }));
    };
    onAmountChange = (event) => {
        const amount = event.target.value;
        if (!amount || amount.match(/^\d{1,}(\.\d{0,2})?$/)) {
            this.setState(() => ({ amount }));
        }
    };
    onNoteChange = (event) => {
        const note = event.target.value;
        this.setState(() => ({note}));
    };
    onDateChange = (createdAt) => {
        if (createdAt){
            this.setState(() => ({ createdAt }))
        }
    };
    onFocuseChange = ({ focused }) => {
        this.setState(() => ({ calenderFocused: focused }))
    };
    onSubmit = (event) => {
        event.preventDefault();
        if (!this.state.description || !this.state.amount) {
            this.setState(() => ({ error: 'Please provide description and amount.' }))
        } else {
            this.setState(() => ({ error: '' }))
            this.props.onSubmit({
                description: this.state.description,
                amount: parseFloat(this.state.amount, 10),
                createdAt: this.state.createdAt.valueOf(),
                note: this.state.note
            })
        }
    };
    render() {
        return (
            <div>
                { this.state.error && <p>{ this.state.error }</p> }
                <form onSubmit={this.onSubmit}>
                    <input 
                        type="text"
                        placeholder="Description"
                        autoFocus
                        value={ this.state.description }
                        onChange={ this.onDescriptionChange }
                    />
                    <input 
                        type="text"
                        placeholder="Amount"
                        value={ this.state.amount }
                        onChange={ this.onAmountChange }
                    />
                    <SingleDatePicker 
                        date={this.state.createdAt}
                        onDateChange={this.onDateChange}
                        focused={this.state.calenderFocused}
                        onFocusChange={this.onFocuseChange}
                        numberOfMonths={1}
                        isOutsideRange={() => false}
                    />
                    <textarea 
                        placeholder="Add note for your expense"
                        value={ this.state.note }
                        onChange={ this.onNoteChange }  
                    ></textarea>
                    <button type="submit">Add expense</button>
                </form>
            </div>
        );
    }
}

export default ExpenseForm;