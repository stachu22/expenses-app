import React from 'react';
import { Link } from 'react-router-dom';

const NotFoundPage = () => (
    <div>
        Page not Found  404! - <Link to="/">Go home</Link>
    </div>
);

export default NotFoundPage;